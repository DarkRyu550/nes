/* mmap.h -- Maps memory from the NES's addresing space
 * to the program's addressing space.
 *
 * The NES is capable of addressing 64KB of memory,
 * however, only the first 2KB of that memory refers
 * to the machine's internal RAM, and the rest is mapped
 * either to other hardware, the cartridge or to as a
 * mirror of a previous memory section, laid out roughly
 * like this: (Credits to the NESdev Wiki)
 * 		<Range>			<Size>	<Description>
 * 		$0000-$07FF		$0800	2KB internal RAM
 *		$0800-$0FFF		$0800	Mirrors of $0000-$07FF
 * 		$1000-$17FF		$0800	
 * 		$1800-$1FFF		$0800	
 * 		$2000-$2007		$0008	NES PPU registers
 * 		$2008-$3FFF		$1FF8	Mirrors of $2000-2007
 * 		$4000-$4017		$0018	NES APU and I/O registers
 * 		$4018-$401F		$0008	Normally disabled func
 * 		$4020-$FFFF		$BFE0	Cartridge space
 *
 * Regions below the cartrige space refer to areas
 * bound to the console's hardware, and thus are
 * trivial to map directly.
 *
 * Cartridge space, in the other hand, due to advances
 * in game production over the console's lifetime, use
 * what are called mappers, chips and extra cartridge
 * circutry that aimed at expanding games beyond the
 * console's original limitations, and due to the large
 * variety in those mappers, it's not as trivial to map
 * addresses correctly.
 **/
#ifndef __MMAP_H__
#define __MMAP_H__

#include <stdio.h>

/* Define a generic template for mapping functions. */
struct memmap;
typedef void*(*mmap_mapper)(struct memmap*, unsigned short);

/* Contains the values needed for memory mapping */
struct memmap{
	/* Start of the NES's 2KB of internal RAM */
	void *ram;

	/* PPU registers */
	void* ppu_reg;

	/* APU and I/O registers */
	void* apu_io_reg;

	/* Normally disabled functions */
	void *debug;

	/* Cartridge data */
	void *cart;

	/* Vector table at the end of
	 * the cartridge space */
	struct{
		short nmi;
		short reset;
		short irq;
	} *vector_table;

	/* Mapper function */
	void*(*mapper)(struct memmap*, unsigned short);
};

/* mmap_resolve() -- Resolves an NES address to the
 * correct target within the emulator.
 *
 * Returns:
 * 		A pointer to an area equivalent to the one
 * 		the provided address would refer to in the
 * 		original hardware, or NULL if none was found.
 * Parameters:
 * 		struct memmap* mmap: The memory map that will be
 *			used for the resolution of the address.
 *		unsigned short address: A memory address in the
 *			NES addressing space.
 */
void*
mmap_resolve(struct memmap* mmap, unsigned short address);

#endif
