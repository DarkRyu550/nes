#include "rp2a03.h"

int
rp2a03_pop(
		struct rp2a03* cpu,
		struct memmap* mmap,
		unsigned char length,
		void *target)
{
	/* Make sure the stack doesn't underflow */
	unsigned int remaining;
	remaining = cpu->bsp.x - cpu->sp.x;
	if(remaining < length){
		fprintf(stderr, 
				"[rp2a03_pop()] Warning: Pop operation would underflow stack\n");
		return -1;
	}

	/* Copy the bytes to the target */
	char *stack;
	stack = mmap_resolve(mmap, cpu->sp.x);

	/* Using memcpy() instead of pulling a value
	 * directly is wasteful if we'll only ever
	 * have to deal with fixed-size values. If by
	 * the end of the implementation of the
	 * instruction set that turns out to be true,
	 * this function will be optimized to handle
	 * only 16-bit values.
	 *
	 * TODO: Re-evaluate whether memcpy()
	 * is needed for rp2a03_pop() and push().
	 */
	memcpy(target, stack, length);

	/* Don't forget to shrink the stack */
	cpu->sp.x += length;

	return length;
}

int
rp2a03_push(
		struct rp2a03* cpu,
		struct memmap* mmap,
		unsigned char length,
		void *source)
{
	/* No overflow bounding control is perfomed here.
	 * Stack overflows might overwrite game code, but
	 * it's not the emulator's job to ensure proper
	 * behaviour of the programs it executes. However,
	 * for debugging purposes, having the stack
	 * go over the border of a page will result in a
	 * message. */
	if(0xff - cpu->sp.i8.l < length)
		fprintf(stderr, "[rp2a03_push()] Stack is pushing over page boundaries\n");

	/* Copy the bytes from the target */
	char *stack;
	stack = mmap_resolve(mmap, cpu->sp.x);

	/* Using memcpy() instead of pulling a value
	 * directly is wasteful if we'll only ever
	 * have to deal with fixed-size values. If by
	 * the end of the implementation of the
	 * instruction set that turns out to be true,
	 * this function will be optimized to handle
	 * only 16-bit values.
	 *
	 * TODO: Re-evaluate whether memcpy()
	 * is needed for rp2a03_pop() and push().
	 */
	memcpy(stack, source, length);

	/* Grow the stack */
	cpu->sp.x += length;

	return length;
}

unsigned int
rp2a03_run(
		struct rp2a03* cpu, 
		struct memmap* memory,
		unsigned int cycles)
{
	/* Timing */
	#define PAGE_CROSS(addr) ((cpu->pc & 0xff00) != (addr & 0xff))
	#define C_NP(x) if(x > cycles){ loop = 0; break; } else cycles -= x;
	#define C_PC(addr, x, y) if(PAGE_CROSS((addr))) { C_NP(x); } else { C_NP((y)); }
	
	/* Convenience macros */
	#define OPR_0   ((char) op->operands & 0xff)
	#define OPR_1   ((char) (op->operands & 0xff00) >> 8)
	#define U_OPR_0 ((unsigned char) op->operands & 0xff)
	#define U_OPR_1 ((unsigned char) (op->operands & 0xff00) >> 8)
	#define U_OPR   ((unsigned short) op->operands)
	#define U_X     ((unsigned char) cpu->x)
	#define U_Y     ((unsigned char) cpu->y)

	/* Addressing mode macros */
	#define ADDR_ZP    (U_OPR_0)
	#define ADDR_ZP_X  (U_OPR_0 + U_X)
	#define ADDR_ZP_Y  (U_OPR_0 + U_Y)
	#define ADDR_ABS   (U_OPR)
	#define ADDR_ABS_X (U_OPR + U_X)
	#define ADDR_ABS_Y (U_OPR + U_Y)
	#define ADDR_IN    (*(short*)mmap_resolve(memory, U_OPR_0))
	#define ADDR_IX    (*(short*)mmap_resolve(memory, (U_OPR_0 + U_X) % 0x100))
	#define ADDR_IY    (*(short*)mmap_resolve(memory, U_OPR_0) + U_Y)

	#define RADDR_ZP    ((char*)mmap_resolve(memory, U_OPR_0))
	#define RADDR_ZP_X  ((char*)mmap_resolve(memory, U_OPR_0 + U_X))
	#define RADDR_ZP_Y  ((char*)mmap_resolve(memory, U_OPR_0 + U_Y))
	#define RADDR_ABS   ((char*)mmap_resolve(memory, U_OPR))
	#define RADDR_ABS_X ((char*)mmap_resolve(memory, U_OPR + U_X))
	#define RADDR_ABS_Y ((char*)mmap_resolve(memory, U_OPR + U_Y))
	#define RADDR_IN    ((char*)mmap_resolve(memory, *(short*)mmap_resolve(memory, U_OPR_0)))
	#define RADDR_IX    ((char*)mmap_resolve(memory, *(short*)mmap_resolve(memory, (U_OPR_0 + U_X) % 0x100)))
	#define RADDR_IY    ((char*)mmap_resolve(memory, *(short*)mmap_resolve(memory, U_OPR_0) + U_Y))

	#define TEST_Z(x) if(x)        cpu->sr &= ~FLAG_ZERO;    else cpu->sr |= FLAG_ZERO; 
	#define TEST_N(x) if(x & 0x80) cpu->sr |= FLAG_NEGATIVE; else cpu->sr &= ~FLAG_NEGATIVE;

	/* Contitional relative branch */
	#define RBRH(condition) \
		if(condition) \
		{ \
			C_PC(5, 3, U_OPR_0); \
			cpu->pc += U_OPR_0; \
		} \
		else \
		{ \
			C_NP(2); \
			incr = 2; \
		}

	/* Acumulator load instruction */
	#define AI(name, i, z, zx, aa, ax, ay, ix, iy, op) \
		case i:  C_NP(2);					incr = 2; m = U_OPR_0; goto name; \
		case z:  C_NP(3);					incr = 2; m = *RADDR_ZP; goto name; \
		case zx: C_NP(4);					incr = 2; m = *RADDR_ZP_X; goto name; \
		case aa: C_NP(4);					incr = 3; m = *RADDR_ABS; goto name; \
		case ax: C_PC(5, 4, ADDR_ABS_X);	incr = 3; m = *RADDR_ABS_X; goto name; \
		case ay: C_PC(5, 4, ADDR_ABS_Y);	incr = 3; m = *RADDR_ABS_Y; goto name; \
		case ix: C_NP(6);					incr = 2; m = *RADDR_IX; goto name; \
		case iy: C_PC(6, 5, ADDR_IY);		incr = 2; m = *RADDR_IY; goto name; \
		name: cpu->a op m; \
				TEST_Z(cpu->a); TEST_N(cpu->a) \
				break;


	/* Retrieve the operation to be executed from
	 * memory. Here the operation is three bytes
	 * long in order to avoid requesting memory
	 * multiple times. */
	struct{
		unsigned char opcode;
		short operands;
	} __attribute__((packed)) *op;
	op = mmap_resolve(memory, cpu->pc);

	char incr = 0, m, *n, loop = 1;
	while(loop){
		/* Loops for as long as possible */
		incr = 0;

		switch(op->opcode){
		/* Conditional relative branching */
		case 0x10: RBRH(~cpu->sr & FLAG_NEGATIVE); break;
		case 0x30: RBRH( cpu->sr & FLAG_NEGATIVE); break;
		case 0x50: RBRH(~cpu->sr & FLAG_OVERFLOW); break;
		case 0x70: RBRH( cpu->sr & FLAG_OVERFLOW); break;
		case 0x90: RBRH(~cpu->sr & FLAG_CARRY);    break;
		case 0xb0: RBRH( cpu->sr & FLAG_CARRY);    break;
		case 0xd0: RBRH(~cpu->sr & FLAG_ZERO);     break;
		case 0xf0: RBRH( cpu->sr & FLAG_ZERO);     break;
		
		/* Accumulator load operations */
		AI(ora, 0x09, 0x05, 0x15, 0x0d, 0x1d, 0x19, 0x01, 0x11, |=); /* ORA */
		AI(and, 0x29, 0x25, 0x35, 0x2d, 0x3d, 0x39, 0x21, 0x31, &=); /* AND */
		AI(eor, 0x49, 0x45, 0x55, 0x4d, 0x5d, 0x59, 0x41, 0x51, ^=); /* EOR */
		AI(lda, 0xa9, 0xa5, 0xb5, 0xad, 0xbd, 0xb9, 0xa1, 0xb1, &=); /* LDA */

		/* JMP -- Unconditional branching */
		case 0x4c: C_NP(3); cpu->pc = U_OPR; break;
		case 0x6c: C_NP(5); cpu->pc = *RADDR_IN; break;
		
		/* LDX -- Load to X register */
		case 0xa2: C_NP(2);					incr = 2; cpu->x = OPR_0; goto ldx;
		case 0xa6: C_NP(3); 				incr = 2; cpu->x = *RADDR_ZP; goto ldx;
		case 0xb6: C_NP(4); 				incr = 2; cpu->x = *RADDR_ZP_Y; goto ldx;
		case 0xae: C_NP(4); 				incr = 3; cpu->x = *RADDR_ABS; goto ldx;
		case 0xbe: C_PC(5, 4, ADDR_ABS_Y);	incr = 3; cpu->x = *RADDR_ABS_Y; goto ldx;
		ldx: TEST_Z(cpu->x); TEST_N(cpu->x); break;
			

		/* LDY -- Load to Y register */
		case 0xa0: C_NP(2);					incr = 2; cpu->y = OPR_0; goto ldy;
		case 0xa4: C_NP(3);					incr = 2; cpu->y = *RADDR_ZP; goto ldy;
		case 0xb4: C_NP(4);					incr = 2; cpu->y = *RADDR_ZP_X; goto ldy;
		case 0xac: C_NP(4);					incr = 3; cpu->y = *RADDR_ABS; goto ldy;
		case 0xbc: C_PC(5, 4, ADDR_ABS_X);	incr = 3; cpu->y = *RADDR_ABS_X; goto ldy;
		ldy: TEST_Z(cpu->x); TEST_N(cpu->x); break;
		
		/* STA -- Store the accomulator in memory */
		case 0x85: C_NP(3); incr = 2; *RADDR_ZP    = cpu->a; break;
		case 0x95: C_NP(4); incr = 2; *RADDR_ZP_X  = cpu->a; break;
		case 0x8d: C_NP(4); incr = 3; *RADDR_ABS   = cpu->a; break;
		case 0x9d: C_NP(5); incr = 3; *RADDR_ABS_X = cpu->a; break;
		case 0x99: C_NP(5); incr = 3; *RADDR_ABS_Y = cpu->a; break;
		case 0x81: C_NP(6); incr = 2; *RADDR_IX    = cpu->a; break;
		case 0x91: C_NP(6); incr = 2; *RADDR_IY    = cpu->a; break;
		
		/* STX -- Store the X register in memory */
		case 0x86: C_NP(3); incr = 2; *RADDR_ZP   = cpu->x; break;
		case 0x96: C_NP(4); incr = 2; *RADDR_ZP_Y = cpu->x; break;
		case 0x8e: C_NP(4); incr = 3; *RADDR_ABS  = cpu->x; break;
		
		/* STY -- Store the Y register in memory */
		case 0x84: C_NP(3); incr = 2; *RADDR_ZP   = cpu->y; break;
		case 0x94: C_NP(4); incr = 2; *RADDR_ZP_X = cpu->y; break;
		case 0x8c: C_NP(4); incr = 3; *RADDR_ABS  = cpu->y; break;

		/* DEC -- Decrement memory */
		case 0xc6: C_NP(5); incr = 2; n = RADDR_ZP;    *n -= 1; goto dec;
		case 0xd6: C_NP(6); incr = 2; n = RADDR_ZP_X;  *n -= 1; goto dec;
		case 0xce: C_NP(6); incr = 3; n = RADDR_ABS;   *n -= 1; goto dec;
		case 0xde: C_NP(7); incr = 3; n = RADDR_ABS_X; *n -= 1; goto dec;
		dec: TEST_Z(*n); TEST_N(*n); break;

		/* DEX -- Decrement X register */
		case 0xca:
			C_NP(2);
			incr = 1;
			cpu->x -= 1;

			TEST_Z(cpu->x);
			TEST_N(cpu->x);

			break;
		
		/* DEY -- Decrement Y register */
		case 0x88: 
			C_NP(2);
			incr = 1;
			cpu->y -= 1;
			
			TEST_Z(cpu->x);
			TEST_N(cpu->x);

			break;

		/* INC -- Increment memory */
		case 0xe6: C_NP(5); incr = 2; n = RADDR_ZP;    *n += 1; goto inc;
		case 0xf6: C_NP(6); incr = 2; n = RADDR_ZP_X;  *n += 1; goto inc;
		case 0xee: C_NP(6); incr = 3; n = RADDR_ABS;   *n += 1; goto inc;
		case 0xfe: C_NP(7); incr = 3; n = RADDR_ABS_X; *n += 1; goto inc;
		inc: TEST_Z(*n); TEST_N(*n); break;

		/* INX -- Increment X register */
		case 0xe8:
			C_NP(2);
			incr = 1;
			cpu->x += 1;
			
			TEST_Z(cpu->x);
			TEST_N(cpu->x);

			break;
		
		/* INY -- Increment Y register */
		case 0xc8: 
			C_NP(2);
			incr = 1;
			cpu->y += 1;
			
			TEST_Z(cpu->x);
			TEST_N(cpu->x);

			break;

		/* ADC -- Add with Carry */
		case 0x69: C_NP(2);					incr = 2; m = OPR_0; goto adc;
		case 0x65: C_NP(3);					incr = 2; m = *RADDR_ZP; goto adc;
		case 0x75: C_NP(4);					incr = 2; m = *RADDR_ZP_X; goto adc;
		case 0x6d: C_NP(4);					incr = 3; m = *RADDR_ABS; goto adc;
		case 0x7d: C_PC(5, 4, ADDR_ABS_X);	incr = 3; m = *RADDR_ABS_X; goto adc;
		case 0x79: C_PC(5, 4, ADDR_ABS_Y);	incr = 3; m = *RADDR_ABS_Y; goto adc;
		case 0x61: C_NP(6);					incr = 2; m = *RADDR_IX; goto adc;
		case 0x71: C_PC(6, 5, ADDR_IY);		incr = 2; m = *RADDR_IY; goto adc;
		adc:{
			short val = ((unsigned char) cpu->a) + m + (cpu->sr & FLAG_CARRY);

			/* Check for the carry */
			if(val & 0xff00) cpu->sr |= FLAG_CARRY; else cpu->sr &= ~FLAG_CARRY;

			/* Check for an overflow */
			if((cpu->a & 0x80 && m & 0x80 && ~val & 0x80)
				|| (~cpu->a & 0x80 && ~m & 0x80 && val & 0x80))
				cpu->sr |= FLAG_OVERFLOW;
			else cpu->sr &= ~FLAG_OVERFLOW;

			/* Check for a negative result */
			if(val & 0x80) cpu->sr |= FLAG_NEGATIVE;
			else cpu->sr &= ~FLAG_NEGATIVE;

			/* Check for zero */
			TEST_Z((val & 0xff));

			cpu->a = (char)(val & 0xff);
			break;
		}

		/* SBC -- Subtract with Carry */
		case 0xe9: C_NP(2);					incr = 2; m = OPR_0; goto sbc;
		case 0xe5: C_NP(3);					incr = 2; m = *RADDR_ZP; goto sbc;
		case 0xf5: C_NP(4);					incr = 2; m = *RADDR_ZP_X; goto sbc;
		case 0xed: C_NP(4);					incr = 3; m = *RADDR_ABS; goto sbc;
		case 0xfd: C_PC(5, 4, ADDR_ABS_X);	incr = 3; m = *RADDR_ABS_X; goto sbc;
		case 0xf9: C_PC(5, 4, ADDR_ABS_Y);	incr = 3; m = *RADDR_ABS_Y; goto sbc;
		case 0xe1: C_NP(6);					incr = 2; m = *RADDR_IX; goto sbc;
		case 0xf1: C_PC(6, 5, ADDR_IY);		incr = 2; m = *RADDR_IY; goto sbc;
		sbc:{
			char val = ((unsigned char) cpu->a) - m - ~(cpu->sr & FLAG_CARRY);

			/* Check for the carry */
			if(val & 0x80) cpu->sr &= ~FLAG_CARRY; else cpu->sr |= FLAG_CARRY;

			/* Check for an overflow */
			if((cpu->a & 0x80 && m & 0x80 && ~val & 0x80)
				|| (~cpu->a & 0x80 && ~m & 0x80 && val & 0x80))
				cpu->sr |= FLAG_OVERFLOW;
			else cpu->sr &= ~FLAG_OVERFLOW;

			/* Check for a negative result */
			if(val & 0x80) cpu->sr |= FLAG_NEGATIVE;
			else cpu->sr &= ~FLAG_NEGATIVE;

			/* Check for zero */
			TEST_Z((val & 0xff));

			cpu->a = val;
			break;
		}
		
		/* ROR -- Rotate right */
		case 0x6a: C_NP(2); incr = 1; n = &cpu->a;    goto ror;
		case 0x66: C_NP(5); incr = 2; n = RADDR_ZP;    goto ror;
		case 0x76: C_NP(6); incr = 2; n = RADDR_ZP_X;  goto ror;
		case 0x6e: C_NP(6); incr = 3; n = RADDR_ABS;   goto ror;
		case 0x7e: C_NP(7); incr = 3; n = RADDR_ABS_X; goto ror;
		ror:{
			/* Save the lsb of the value to be rotated,
			 * as it will be saved as the carry later on.
			 */
			char nc = *n & 1;

			/* Shift the value by one bit */
			*n >>= 1;

			/* Save the current carry bit as the msb
			 * of the freshly shifted value */
			*n |= (cpu->sr & FLAG_CARRY) << 7;

			/* Set the negative flag */
			if(cpu->sr & FLAG_CARRY)
				cpu->sr |= FLAG_NEGATIVE;
			else
				cpu->sr &= ~FLAG_NEGATIVE;

			/* Set the new carry */
			cpu->sr &= ~FLAG_CARRY;
			cpu->sr |= nc;

			if(*n) cpu->sr &= ~FLAG_ZERO;
			else cpu->sr |= FLAG_ZERO;

			break;
		}
		
		/* ROL -- Rotate left */
		case 0x2a: C_NP(2); incr = 1; n = &cpu->a;    goto rol;
		case 0x26: C_NP(5); incr = 2; n = RADDR_ZP;    goto rol;
		case 0x36: C_NP(6); incr = 2; n = RADDR_ZP_X;  goto rol;
		case 0x2e: C_NP(6); incr = 3; n = RADDR_ABS;   goto rol;
		case 0x3e: C_NP(7); incr = 3; n = RADDR_ABS_X; goto rol;
		rol:{
			/* Save the msb of the value to be rotated,
			 * as it will be saved as the carry later on.
			 */
			char nc = (*n & 0x80) >> 7;

			/* Shift the value by one bit */
			*n <<= 1;

			/* Save the current carry bit as the lsb
			 * of the freshly shifted value */
			*n |= cpu->sr & FLAG_CARRY;

			/* Set the negative flag */
			if(*n & 0x80)
				cpu->sr |= FLAG_NEGATIVE;
			else
				cpu->sr &= ~FLAG_NEGATIVE;

			/* Set the new carry */
			cpu->sr &= ~FLAG_CARRY;
			cpu->sr |= nc;

			if(*n) cpu->sr &= ~FLAG_ZERO;
			else cpu->sr |= FLAG_ZERO;

			break;
		}

		/* ASL -- Logical shift left */
		case 0x0a: C_NP(2); incr = 1; n = &cpu->a;    goto asl;
		case 0x06: C_NP(5); incr = 2; n = RADDR_ZP;    goto asl;
		case 0x16: C_NP(6); incr = 2; n = RADDR_ZP_X;  goto asl;
		case 0x0e: C_NP(6); incr = 3; n = RADDR_ABS;   goto asl;
		case 0x1e: C_NP(7); incr = 3; n = RADDR_ABS_X; goto asl;
		asl:{
			/* Save the msb of the value to be shifted,
			 * as it will be saved as the carry later on.
			 */
			char nc = (*n & 0x80) >> 7;

			/* Shift the value by one bit */
			*n <<= 1;

			/* Set the negative flag */
			if(*n & 0x80)
				cpu->sr |= FLAG_NEGATIVE;
			else
				cpu->sr &= ~FLAG_NEGATIVE;

			/* Set the new carry */
			cpu->sr &= ~FLAG_CARRY;
			cpu->sr |= nc;

			if(*n) cpu->sr &= ~FLAG_ZERO;
			else cpu->sr |= FLAG_ZERO;

			break;
		}

		/* LSR -- Logical shift right */
		case 0x4a: C_NP(2); incr = 1; n = &cpu->a;    goto lsr;
		case 0x46: C_NP(5); incr = 2; n = RADDR_ZP;    goto lsr;
		case 0x56: C_NP(6); incr = 2; n = RADDR_ZP_X;  goto lsr;
		case 0x4e: C_NP(6); incr = 3; n = RADDR_ABS;   goto lsr;
		case 0x5e: C_NP(7); incr = 3; n = RADDR_ABS_X; goto lsr;
		lsr:{
			/* Save the lsb of the value to be shifted,
			 * as it will be saved as the carry later on.
			 */
			char nc = *n & 1;

			/* Shift the value by one bit */
			*n >>= 1;

			/* Clear the negative flag */
			cpu->sr &= ~FLAG_NEGATIVE;

			/* Set the new carry */
			cpu->sr &= ~FLAG_CARRY;
			cpu->sr |= nc;

			if(*n) cpu->sr &= ~FLAG_ZERO;
			else cpu->sr |= FLAG_ZERO;

			break;
		}

		/* BIT - AND's a memory value against A */
		case 0x24: C_NP(3); incr = 2; m = *RADDR_ZP;  goto bit;
		case 0x2c: C_NP(4); incr = 3; m = *RADDR_ABS; goto bit;
		bit:{
			char result = m & cpu->a;

			if(result)
				cpu->sr &= ~FLAG_ZERO;
			else 
				cpu->sr |= FLAG_ZERO;

			cpu->sr &= ~(FLAG_OVERFLOW | FLAG_NEGATIVE);
			cpu->sr |= m & 0xc0;
			break;
		}
		
		/* CMP -- Compares memory and the accumulator */
		case 0xc9: C_NP(2);					incr = 2; m = OPR_0; goto cmp;
		case 0xc5: C_NP(3);					incr = 2; m = *RADDR_ZP; goto cmp;
		case 0xd5: C_NP(4);					incr = 2; m = *RADDR_ZP_X; goto cmp;
		case 0xcd: C_NP(4);					incr = 3; m = *RADDR_ABS; goto cmp;
		case 0xdd: C_PC(5, 4, ADDR_ABS_X);	incr = 3; m = *RADDR_ABS_X; goto cmp;
		case 0xd9: C_PC(5, 4, ADDR_ABS_Y);	incr = 3; m = *RADDR_ABS_Y; goto cmp;
		case 0xc1: C_NP(6);					incr = 2; m = *RADDR_IX; goto cmp;
		case 0xd1: C_PC(6, 5, ADDR_IY);		incr = 2; m = *RADDR_IY; goto cmp;
		cmp:{
			char val = ((unsigned char) cpu->a) - m;

			/* Check for the carry */
			if(val & 0x80) cpu->sr &= ~FLAG_CARRY; else cpu->sr |= FLAG_CARRY;

			/* Check for a negative result */
			if(val & 0x80) cpu->sr |= FLAG_NEGATIVE;
			else cpu->sr &= ~FLAG_NEGATIVE;

			/* Check for zero */
			TEST_Z(val);

			break;
		}

		/* CPX -- Compares memory and the X register */
		case 0xe0: C_NP(2); incr = 2; m = OPR_0; goto cpx;
		case 0xe4: C_NP(3); incr = 2; m = *RADDR_ZP; goto cpx;
		case 0xec: C_NP(4); incr = 3; m = *RADDR_ABS; goto cpx;
		cpx:{
			char val = ((unsigned char) cpu->x) - m;

			/* Check for the carry */
			if(val & 0x80) cpu->sr &= ~FLAG_CARRY; else cpu->sr |= FLAG_CARRY;

			/* Check for a negative result */
			if(val & 0x80) cpu->sr |= FLAG_NEGATIVE;
			else cpu->sr &= ~FLAG_NEGATIVE;

			/* Check for zero */
			TEST_Z(val);

			break;
		}

		/* CPY -- Compares memory and the Y register */
		case 0xc0: C_NP(2); incr = 2; m = OPR_0; goto cpy;
		case 0xc4: C_NP(3); incr = 2; m = *RADDR_ZP; goto cpy;
		case 0xcc: C_NP(4); incr = 3; m = *RADDR_ABS; goto cpy;
		cpy:{
			char val = ((unsigned char) cpu->y) - m;

			/* Check for the carry */
			if(val & 0x80) cpu->sr &= ~FLAG_CARRY; else cpu->sr |= FLAG_CARRY;

			/* Check for a negative result */
			if(val & 0x80) cpu->sr |= FLAG_NEGATIVE;
			else cpu->sr &= ~FLAG_NEGATIVE;

			/* Check for zero */
			TEST_Z(val);

			break;
		}

		/* Stack operations */
		case 0x08: /* PHP */ C_NP(3); incr = 1; rp2a03_push(cpu, memory, 1, &cpu->sr); break;
		case 0x48: /* PHA */ C_NP(3); incr = 1; rp2a03_push(cpu, memory, 1, &cpu->a);  break;
		case 0x28: /* PLP */ C_NP(4); incr = 1; rp2a03_pop(cpu, memory, 1, &cpu->sr); break;
		case 0x68: /* PLA */ C_NP(4); incr = 1; rp2a03_pop(cpu, memory, 1, &cpu->a);  break;

		/* Flag operations */
		case 0x38: /* SEC */ C_NP(2); incr = 1; cpu->sr |=  FLAG_CARRY; break;
		case 0xf8: /* SED */ C_NP(2); incr = 1; cpu->sr |=  FLAG_BCD; break;
		case 0x78: /* SEI */ C_NP(2); incr = 1; cpu->sr |=  FLAG_INTINHIBIT; break;
		case 0x18: /* CLC */ C_NP(2); incr = 1; cpu->sr &= ~FLAG_CARRY; break;
		case 0xd8: /* CLD */ C_NP(2); incr = 1; cpu->sr &= ~FLAG_BCD; break;
		case 0x58: /* CLI */ C_NP(2); incr = 1; cpu->sr &= ~FLAG_INTINHIBIT; break;
		case 0xb8: /* CLV */ C_NP(2); incr = 1; cpu->sr &= ~FLAG_OVERFLOW; break;

		/* Transfers */
		case 0xa8: /* TAY */ C_NP(2); incr = 1; cpu->y  = cpu->a;  break;
		case 0x98: /* TYA */ C_NP(2); incr = 1; cpu->a  = cpu->y;  break;
		case 0x8a: /* TXA */ C_NP(2); incr = 1; cpu->a  = cpu->x;  break;
		case 0xaa: /* TAX */ C_NP(2); incr = 1; cpu->x  = cpu->a;  break;
		case 0xba: /* TSX */ C_NP(2); incr = 1; cpu->x  = cpu->sp.i8.l; break;
		case 0x9a: /* TXS */ C_NP(2); incr = 1; cpu->sp.i8.l = cpu->x;  break;

		/* NOP */
		case 0xea: C_NP(2); incr = 1; break;

		case 0x00:
			/* TODO: BRK -- Trigger an unmaskable interrupt */
			C_NP(7);
			incr = 1;
			break;
		case 0x20:{
			/* JSR abs -- Jump to subroutine at an absolutely
			 * addressed location and push (PC - 1) onto the stack */
			C_NP(6);
			short addr = cpu->pc - 1;
			rp2a03_push(cpu, memory, 2, &addr);

			cpu->pc = op->operands;
			break;
			}
		case 0x40:
			/* TODO: RTI -- Return from interrupt */
			C_NP(6);
			incr = 1;
			break;
		case 0x60:{
			/* RTS -- Return from subroutine to the address
			 * stored atop the stack plus one. */
			C_NP(6);
			short addr;
			rp2a03_pop(cpu, memory, 2, &addr);

			cpu->pc = addr;
			break;
			}
		} /* Switch */

		/* Increment the program counter */
		cpu->pc = incr;
	} /* Loop */

}

