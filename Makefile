CC=cc
CFLAGS=-O0 -g
LDFLAGS=

OBJS=mmap.o ines.o mappers.o rp2a03.o nes.o


nes: $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $(OBJS)

%.o: %.c %.h
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -rf *.o nes

